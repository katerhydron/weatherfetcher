//
//  NetworkManagerTests.swift
//  WeatherFetcher
//
//  Created by Kamil Kwiatkowski on 12/06/2017.
//  Copyright © 2017 Kamil Kwiatkowski. All rights reserved.
//

import XCTest
@testable import WeatherFetcher

class NetworkManagerTests: XCTestCase, NetworkProtocol {
    
    var expectation : XCTestExpectation?
    
    override func setUp() {
        super.setUp()
        
        expectation = self.expectation(description: "Asynchronous expectation")
    }
    
    override func tearDown() {
        
        expectation = nil
        
        super.tearDown()
    }
    
    func testFetchOneCity() {

        let networkManager = NetworkManager()
        networkManager.delegate = self
        
        networkManager.fetchWeatherForCity(cityName: "Warsaw")
        
        self.waitForExpectations(timeout: 2.0, handler: nil)
    }
    
    func didCompleteRequest(parsedCities : (Array<City>)) {
        expectation?.fulfill()
    }
    
    func didFailedRequest() {
        
    }

    
}
