//
//  ViewControllerTests.swift
//  ViewControllerTests
//
//  Created by Kamil Kwiatkowski on 10.06.2017.
//  Copyright © 2017 Kamil Kwiatkowski. All rights reserved.
//

import UIKit
import Foundation
import XCTest
@testable import WeatherFetcher

class ViewControllerTests: XCTestCase {
    
    var viewController : ViewController!
    var networkManagerStub : NetworkManagerStub!
    
    class NetworkManagerStub: NetworkManager {
        var fetchWeatherForGroupCalled: Bool = false
        
        override func fetchWeatherForGroup(cityGroup : Array<String>) {
            fetchWeatherForGroupCalled = true
        }
    }
    
    override func setUp() {
        super.setUp()
        
        let navigationController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as! UINavigationController
        viewController = navigationController.topViewController as! ViewController
        UIApplication.shared.keyWindow!.rootViewController = viewController
        
        // The One Weird Trick! https://www.natashatherobot.com/ios-testing-view-controllers-swift/
        let _ = navigationController.view
        let _ = viewController.view
        
        networkManagerStub =  NetworkManagerStub()
        //viewController.networkManager = networkManagerStub
    }
    
    override func tearDown() {
        
        viewController = nil
        super.tearDown()
    }
    
    func testExample() {
        
        XCTAssertNotNil(viewController.tableView.dataSource)
        XCTAssertNotNil(viewController.tableView.delegate)
        XCTAssertEqual(viewController.cities.count, 0)
    }
}
