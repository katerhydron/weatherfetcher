//
//  DIDetailViewControllerTests.swift
//  WeatherFetcher
//
//  Created by Kamil Kwiatkowski on 13/06/2017.
//  Copyright © 2017 Kamil Kwiatkowski. All rights reserved.
//

import XCTest
@testable import WeatherFetcher

class DIDetailViewControllerTests: XCTestCase {
    
    var viewController: DIDetailViewController!
    var networkManager: NetworkManager!
    var networkManagerStub: NetworkManagerStub!
    
    class NetworkManagerStub: NetworkManager {
        var fetchWeatherForCityCalled: Bool = false
        
        override func fetchWeatherForCity(cityName: String) {
            fetchWeatherForCityCalled = true
        }
    }

    class SpyNetworkDelegate: NetworkProtocol {
        
        var somethingWithDelegateAsyncResult: Bool?
        var asyncExpectation: XCTestExpectation?
        //var completionHandler: ((Array<City>)->Void)?
        
        func didCompleteRequest(parsedCities: Array<City>) {
            guard let expectation = asyncExpectation else {
                XCTFail("SpyDelegate was not setup correctly. Missing XCTExpectation reference")
                return
            }
            somethingWithDelegateAsyncResult = true
            expectation.fulfill()
        }
        
        func didFailedRequest() {
            
        }
    }
    
    override func setUp() {
        super.setUp()
        
        networkManager = NetworkManager.sharedInstance
    }
    
    override func tearDown() {
        super.tearDown()
        
        viewController = nil
        networkManager = nil
        networkManagerStub = nil
    }
    
    func testNetworkManagerCalls() {
        
        networkManagerStub = NetworkManagerStub()
        viewController = DIDetailViewController.initDetailsVC(cityName: "Lodz", networkManager: networkManagerStub)
        
        // triggers life cycle methods
        let _ = viewController.view
        
        XCTAssertTrue(networkManagerStub.fetchWeatherForCityCalled)
    }
    
    func testNetworkManagerResponse() {
        
        viewController = DIDetailViewController.initDetailsVC(cityName: "Lodz", networkManager: networkManager)
        let spyDelegate = SpyNetworkDelegate()
        spyDelegate.asyncExpectation = expectation(description: "NetworkManager calls the delegate as the result of an async method completion")
        
        // triggers life cycle methods
        let _ = viewController.view
        
        // inject spy delegate after viewDidLoad
        networkManager.delegate = spyDelegate
        
        waitForExpectations(timeout: 5) { (error) in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            
            guard spyDelegate.somethingWithDelegateAsyncResult != nil else {
                XCTFail("Expected delegate to be called")
                return
            }
        }
    }
}
