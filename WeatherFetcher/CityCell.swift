//
//  CityCell.swift
//  WeatherFetcher
//
//  Created by Kamil Kwiatkowski on 10.06.2017.
//  Copyright © 2017 Kamil Kwiatkowski. All rights reserved.
//

import UIKit

class CityCell: UITableViewCell {

    // MARK Properties
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
}
