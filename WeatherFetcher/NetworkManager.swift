//
//  NetworkManager.swift
//  WeatherFetcher
//
//  Created by Kamil Kwiatkowski on 10.06.2017.
//  Copyright © 2017 Kamil Kwiatkowski. All rights reserved.
//

import UIKit

protocol NetworkProtocol {
    
    func didCompleteRequest(parsedCities : Array<City>)
    func didFailedRequest()
}

struct Constants {
    
    static let kHostUrl = "http://api.openweathermap.org/data/2.5/"
    static let kGroupId = "group?id="
    static let kSingleCityId = "weather?id="
    static let kUnitsMetricParam = "&units=metric"
    static let kApiKeyParam = "&APPID="
    static let kOpenWeatherMapAPIKey = "577258965c20511e11500617d7b1b025"
    static let kCityNameLodz = "Lodz"
    static let kCityNameBydgoszcz = "Bydgoszcz"
    static let kCityNameWarsaw = "Warsaw"
    
    static let cityIds = [ kCityNameLodz : 3093133,
                           kCityNameWarsaw : 4927854,
                           kCityNameBydgoszcz : 3102014 ]
}

class NetworkManager: NSObject {
    
    var delegate : NetworkProtocol?
    let jsonManager = JSONManager()
    
    static let sharedInstance: NetworkManager = {
        let instance = NetworkManager()
        return instance
    }()
    
    func fetchWeatherForCity(cityName : String) {
        
        let cityId = String(describing: Constants.cityIds[cityName]!)
        let parameter = Constants.kSingleCityId + cityId
        let url = self.urlWithParameter(parameter: parameter)
        
        self.fetchDataFromUrl(url: url, completionHandler: {json in
            let oneCityArray = self.jsonManager.oneCityFromJson(json: json)
            self.delegate?.didCompleteRequest(parsedCities: oneCityArray)
        })
    }
    
    func fetchWeatherForGroup(cityGroup : Array<String>) {
        
        var groupedIds = [String]()
        for cityName in cityGroup {
            groupedIds.append(String(describing: Constants.cityIds[cityName]!))
        }
        
        let parameter = Constants.kGroupId + groupedIds.joined(separator: ",")
        let url = self.urlWithParameter(parameter: parameter)
        
        self.fetchDataFromUrl(url: url, completionHandler: { json in
            let citiesArray = self.jsonManager.citiesFromJSON(json: json)
            self.delegate?.didCompleteRequest(parsedCities: citiesArray)
        })
    }
    
    private func fetchDataFromUrl(url : URL, completionHandler:@escaping (_ json : [String:Any]) -> ()) {
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: url) { (data, response, error) in
            if error != nil {
                self.delegate?.didFailedRequest()
                print(error!.localizedDescription)
            } else {
                let json = try? JSONSerialization.jsonObject(with: data!) as! [String : Any]
                completionHandler(json!)
            }
        }
        task.resume()
    }
    
    
    private func urlWithParameter(parameter : String) -> (URL) {
        
        return URL(string : Constants.kHostUrl + parameter + Constants.kApiKeyParam + Constants.kOpenWeatherMapAPIKey + Constants.kUnitsMetricParam)!
    }
}
