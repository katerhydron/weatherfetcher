//
//  ViewController.swift
//  WeatherFetcher
//
//  Created by Kamil Kwiatkowski on 10.06.2017.
//  Copyright © 2017 Kamil Kwiatkowski. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, NetworkProtocol {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var cities = [City]()
    var networkManager : NetworkManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //unable to test it!
        networkManager = NetworkManager.sharedInstance
        tableView.dataSource = self
        tableView.delegate = self
        spinner.startAnimating()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        networkManager?.delegate = self
        networkManager?.fetchWeatherForGroup(cityGroup: [Constants.kCityNameLodz,
                                                                       Constants.kCityNameWarsaw,
                                                                       Constants.kCityNameBydgoszcz])
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath) as! CityCell
        let city = cities[indexPath.row]
        cell.cityLabel.text = city.name as String
        cell.temperatureLabel.text = String(describing: city.temperature!)
        
        return cell
    }
    
    // MARK - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cityName = cities[indexPath.row].name!
        let detailsVC = DIDetailViewController.initDetailsVC(cityName: cityName, networkManager: NetworkManager.sharedInstance)
        self.navigationController?.pushViewController(detailsVC, animated: true)
    }
    
    // MARK NetworkProtocol delegate methods
    
    func didCompleteRequest(parsedCities: Array<City>) {
        self.stopSpinner()
        cities = parsedCities
        DispatchQueue.main.sync {
            self.tableView.reloadData()
        }
    }
    
    func didFailedRequest() {
        self.stopSpinner()
        self.showFailAlert()
    }
    
    // MARK UI
    
    func stopSpinner() {
        DispatchQueue.main.sync {
            spinner.stopAnimating()
        }
    }
    
    func showFailAlert() {
        let alert = UIAlertController(title: "Sorry,", message: "Downloading weather has failed", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

