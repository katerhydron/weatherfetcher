//
//  JSONManager.swift
//  WeatherFetcher
//
//  Created by Kamil Kwiatkowski on 11/06/2017.
//  Copyright © 2017 Kamil Kwiatkowski. All rights reserved.
//

import UIKit

class JSONManager: NSObject {

    func citiesFromJSON(json : [String:Any]) -> (Array<City>) {
        
        var citiesArray = [City]()
        let list : NSArray = json["list"] as! NSArray
        for case let cityForecast as [String:Any] in list {
            let cityName = cityForecast["name"] as! String
            let mainInfo = cityForecast["main"] as! [String:Any]
            let tempFloat : Float = mainInfo["temp"] as! Float
            let temp = String(format: "%.01f °C", tempFloat)
            let city = City(name: cityName, temp: temp)
            citiesArray.append(city)
            // #learningswift
        }

        return citiesArray
    }
    
    func oneCityFromJson(json : [String:Any]) -> (Array<City>) {
        
        var citiesArray = [City]()
        
        let cityName = json["name"] as! String
        let mainInfo = json["main"] as! [String:Any]
        let tempFloat : Float = mainInfo["temp"] as! Float
        let temp = String(format: "%.01f °C", tempFloat)
        let city = City(name: cityName, temp: temp)
        let pressureFloat = mainInfo["pressure"] as! Float
        city.pressure = String(format: "%.0f HPa", pressureFloat)
        let weather = json["weather"] as! Array<[String:Any]>
        let conditions = weather.first!
        city.conditionsDescription = conditions["description"] as? String
        
        citiesArray.append(city)
        
        return citiesArray
    }
}
