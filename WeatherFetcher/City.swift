//
//  City.swift
//  WeatherFetcher
//
//  Created by Kamil Kwiatkowski on 11/06/2017.
//  Copyright © 2017 Kamil Kwiatkowski. All rights reserved.
//

import UIKit

class City: NSObject {
    var name : String!
    var temperature : String!
    var pressure : String?
    var conditionsDescription : String?
    
    init(name: String, temp: String) {
        self.name = name
        self.temperature = temp
    }

}
