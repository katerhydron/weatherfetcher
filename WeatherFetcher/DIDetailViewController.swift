//
//  DIDetailViewController.swift
//  WeatherFetcher
//
//  Created by Kamil Kwiatkowski on 12/06/2017.
//  Copyright © 2017 Kamil Kwiatkowski. All rights reserved.
//

import UIKit

class DIDetailViewController: UIViewController, NetworkProtocol {

    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    
    var networkManager : NetworkManager = NetworkManager.sharedInstance
    var cityNameString : String?
    
    static func initDetailsVC(cityName:String, networkManager:NetworkManager) -> DIDetailViewController {
        let detailsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailsVC") as! DIDetailViewController

        networkManager.delegate = detailsVC
        detailsVC.networkManager = networkManager
        detailsVC.cityNameString = cityName
        return detailsVC
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        networkManager.fetchWeatherForCity(cityName: self.cityNameString!)
    }

    func didCompleteRequest(parsedCities : Array<City>) {
        let city = parsedCities.first
        self.refreshUIWithCity(city : city!)
    }
    
    func didFailedRequest() {
        print("Download failed")
        self.navigationController?.popViewController(animated: true)
    }
    
    func refreshUIWithCity(city : City) {
        DispatchQueue.main.sync {
            self.cityName.text = city.name
            self.tempLabel.text = city.temperature
            self.pressureLabel.text = city.pressure
            self.descriptionLabel.text = city.conditionsDescription
            self.view.setNeedsDisplay()
        }
    }
}
